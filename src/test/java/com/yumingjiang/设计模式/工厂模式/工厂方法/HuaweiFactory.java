package com.yumingjiang.设计模式.工厂模式.工厂方法;

public class HuaweiFactory implements IMethodFactory<Huawei> {
    @Override
    public Huawei build() {
        return new Huawei();
    }
}

