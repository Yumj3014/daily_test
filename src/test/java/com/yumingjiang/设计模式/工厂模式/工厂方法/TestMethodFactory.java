package com.yumingjiang.设计模式.工厂模式.工厂方法;

public class TestMethodFactory {
    public static void main(String[] args) {
        //创建huawei
        Huawei huawei = new HuaweiFactory().build();
        //创建apple
        Apple apple = new AppleFactory().build();
    }
}