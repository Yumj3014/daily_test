package com.yumingjiang.设计模式.工厂模式.简单工厂;

import org.junit.Test;

/**
 * 测试简单工厂
 */
public class SimpleFactoryTest {

    @Test
    public void testSimpleFactory(){
        Object instance1 = SimpleFactory.getINSTANCE(TUser.class);
        Object instance2 = SimpleFactory.getINSTANCE(TUser.class);
        System.out.println(instance1);
        System.out.println(instance2);
        System.out.println(instance2==instance1);
    }
}
