package com.yumingjiang.设计模式.工厂模式.简单工厂;

import lombok.Data;

@Data
public class TUser {
    private int id;
    public String name;
}
