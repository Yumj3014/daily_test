package com.yumingjiang.设计模式.工厂模式.简单工厂;

/**
 * 简单工厂
 */
public class SimpleFactory {

    /**
     * 防止单例被破坏
     */
    private SimpleFactory(){
        //工厂必须是单例的。
        //加这行代码的原因考虑到反射能够改变修饰符的原因
        if (InnerObjectFactory.INSTANCE!=null){
            throw new RuntimeException("### 当前实例对象已被创建。INSTANCE："+InnerObjectFactory.INSTANCE);
        }
    }


    /**
     * 静态内部类保证单例
     */
    private static class InnerObjectFactory{
        public static SimpleFactory INSTANCE = new SimpleFactory();
    }

    /**
     * 工厂方法
     *
     * 注意工厂是单例的，但是工厂方法的对象是多例的别搞乱了。
     * @param clazz
     * @return
     */
    public static Object getINSTANCE(Class<?> clazz){
        try {
            return clazz.newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }
}
