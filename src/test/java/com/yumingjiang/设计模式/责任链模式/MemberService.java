package com.yumingjiang.设计模式.责任链模式;

public interface MemberService {
    void login(Member member);
}