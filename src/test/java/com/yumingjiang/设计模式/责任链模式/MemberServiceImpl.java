package com.yumingjiang.设计模式.责任链模式;

public class MemberServiceImpl implements MemberService{
    @Override
    public void login(Member member) {
        AuthHandle authHandle = new AuthHandle();
        LoginHandle loginHandle = new LoginHandle();
        ValidateHandle validateHandle = new ValidateHandle();
        validateHandle.next(loginHandle);
        loginHandle.next(authHandle);
        validateHandle.doHandle(member);
    }
}