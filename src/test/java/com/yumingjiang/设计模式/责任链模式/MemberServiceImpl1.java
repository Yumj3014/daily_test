package com.yumingjiang.设计模式.责任链模式;

public class MemberServiceImpl1 implements MemberService{
    @Override
    public void login(Member member) {
        Handle.Builder builder = new Handle.Builder();
        builder.addHandle(new ValidateHandle())
                .addHandle(new LoginHandle())
                .addHandle(new AuthHandle());
        builder.build().doHandle(member);

    }
}