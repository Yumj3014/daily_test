package com.yumingjiang.设计模式.责任链模式;

public class LoginHandle extends Handle{
    @Override
    public void doHandle(Member member) {
        System.out.println("登录成功");
        chain.doHandle(member);
    }
}