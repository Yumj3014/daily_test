package com.yumingjiang.设计模式.责任链模式;

public class ValidateHandle extends Handle{
    @Override
    public void doHandle(Member member) {
        if (member.getLoginName()==null||member.getLoginPass()==null){
            System.out.println("账户名或密码为空");
            return;
        }
        System.out.println("账户名或密码校验成功，继续往下执行");
        chain.doHandle(member);
    }
}