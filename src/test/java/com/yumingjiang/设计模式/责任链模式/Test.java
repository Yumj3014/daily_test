package com.yumingjiang.设计模式.责任链模式;

public class Test {
    public static void main(String[] args) {
        Member member = new Member("root", "root", "admin");
        MemberService service = new MemberServiceImpl();
        MemberService service1 = new MemberServiceImpl1();
        service.login(member);
        System.out.println("====================*====================");
        service1.login(member);
    }
}