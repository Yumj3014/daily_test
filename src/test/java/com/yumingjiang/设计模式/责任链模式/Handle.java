package com.yumingjiang.设计模式.责任链模式;

public abstract class Handle<T> {
    protected Handle chain;

    protected void next(Handle chain) {
        this.chain=chain;
    }
    public abstract void doHandle(Member member);

    public static class Builder<T>{
        private Handle<T> head;
        private Handle<T> tail;

        public Builder<T> addHandle(Handle<T> handle){
            if (this.tail==null){
                this.head=this.tail=handle;
                return this;
            }
            this.head.next(handle);
            this.tail=handle;

            return this;
        }

        public Handle<T> build(){
            return this.head;
        }
    }
}
