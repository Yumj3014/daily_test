package com.yumingjiang.设计模式.责任链模式;

public class AuthHandle extends Handle{
    @Override
    public void doHandle(Member member) {
        if (!member.getRoleName().equals("admin")){
            System.out.println("角色验证失败....");
            return;
        }
        System.out.println("欢迎您登录，管理元");
    }
}