package com.yumingjiang.设计模式.单例模式;

/**
 * <h3>study</h3>
 * 注册式单例
 * 优点：不能被反射破坏，但是能够被序列化破坏
 * @author : 余明江
 * @date : 2021-03-20 03:42
 **/
public enum EnumSingleton {
    INSTANCE;
    private String data;

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
    public static EnumSingleton getInstance(){
        return INSTANCE;
    }
}