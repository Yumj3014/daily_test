package com.yumingjiang.设计模式.单例模式;

public class SimpleLazySingleton03 {
    /**
     * 私有构造参数
     */
    private SimpleLazySingleton03(){
        if (LazyClass.INSTANCE!=null){
            throw new RuntimeException("### 当前单例对象已被创建，请勿重复创建。INSTANCE:"+getInstance());
        }
    }

    //每一个关键字都不是多余的，static是使单例的空间共享，final保证这个方法不会被重写、重载
    public static final SimpleLazySingleton03 getInstance(){
        //在返回结果以前一定会先加载内部类
        return LazyClass.INSTANCE;
    }
    /**
     * 默认不加载
     */
    private static class LazyClass{
        private static SimpleLazySingleton03 INSTANCE = new SimpleLazySingleton03();
    }
}
