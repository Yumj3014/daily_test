package com.yumingjiang.设计模式.单例模式;

/**
 * 单例模式之饿汉式
 * 标准饿汉式代码
 */
public class HangrySingleton01 {
    private static HangrySingleton01 INSTENCE = new HangrySingleton01();

    private HangrySingleton01(){}

    public static HangrySingleton01 getINSTENCE() {
        return INSTENCE;
    }
}
