package com.yumingjiang.设计模式.单例模式;

/**
 * 单例模式之饿汉式
 * 标准饿汉式代码
 *
 * 这两种写法都非常简单、也非常好理解，但是在单例模式适用于单例对象较少的情况。这样写可以保证绝对的线程安全，
 * 执行效率比较高，但是他的缺点也比较明显，
 * 就是所有的对象在类加载的时候就完成了实例化，这样内存中就存在这很多的空闲对象，浪费了内存。
 */
public class HangrySingleton02 {
    private static HangrySingleton02 INSTANCE;
    static {
        INSTANCE = new HangrySingleton02();
    }
    private HangrySingleton02(){}

    public static HangrySingleton02 getINSTANCE() {
        return INSTANCE;
    }
}
