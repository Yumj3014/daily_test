package com.yumingjiang.设计模式.单例模式;

/**
 * 单例模式之懒汉式简单写法
 *
 * 多线程情况下会出现线程安全的问题
 */
public class SimpleLazySingleton01 {
    private static SimpleLazySingleton01 INSTANCE;

    private SimpleLazySingleton01(){}

    public static SimpleLazySingleton01 getINSTANCE() {
        return INSTANCE = new SimpleLazySingleton01();
    }
}
