package com.yumingjiang.设计模式.单例模式;

/**
 * 单例模式之懒汉式简单写法
 *
 * 因为有锁的缘故，所以在执行效率方面还是有这一定的局限
 */
public class SimpleLazySingleton02 {
    private static SimpleLazySingleton02 INSTANCE;
    private SimpleLazySingleton02(){}

    public static SimpleLazySingleton02 getINSTANCE() {
        if (INSTANCE==null){
            synchronized (SimpleLazySingleton02.class){
                if (INSTANCE==null){
                    return INSTANCE = new SimpleLazySingleton02();
                }
            }
        }
        return null;
    }
}
