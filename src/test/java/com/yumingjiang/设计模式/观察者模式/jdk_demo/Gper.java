package com.yumingjiang.设计模式.观察者模式.jdk_demo;

import java.util.Observable;

/**
 * 被监听者
 * 使用jdk自带的工具实现观察者模式
 */
public class Gper extends Observable {
    private String name="Gper生态圈";
    private static final Gper gper = new Gper();
    private Gper(){}

    public static Gper getIntance() {
        return gper;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void publishQuestion(Question question){
        System.out.println(question.getUsername()+"在"+this.name+"提出了一个问题.");
        //发布消息
        setChanged();
        //通知所有的订阅者
        notifyObservers(question);
    }
}