package com.yumingjiang.设计模式.观察者模式.jdk_demo;

public class Test {
    public static void main(String[] args) {
        //被监听者
        Gper gper = Gper.getIntance();
        //订阅者（监听者）
        Teacher zhangsan = new Teacher("zhangsan");
        Teacher lisi = new Teacher("lisi");
        gper.addObserver(zhangsan);
        gper.addObserver(lisi);

        //用户行为
        Question question = new Question();
        question.setUsername("Tom");
        question.setContent("观察者模式适用于哪些场景？");

        gper.publishQuestion(question);
    }
}