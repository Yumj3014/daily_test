package com.yumingjiang.设计模式.观察者模式.jdk_demo;

import java.util.Observable;
import java.util.Observer;

/**
 * 观察者
 */
public class Teacher implements Observer {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Teacher(String name) {
        this.name = name;
    }

    @Override
    public void update(Observable o, Object arg) {
        Gper gper = (Gper) o;
        Question question = (Question) arg;
        System.out.println("==========================");
        System.out.println(name+"老师、你好 \n" +
                "您收到了一个来自"+gper.getName()+"的题问，希望您解答。问题内容如下：\n"+question.getContent() +"\n"+
                "提问者："+question.getUsername() );
    }
}