package com.yumingjiang.设计模式.观察者模式.juava_demo;

import com.google.common.eventbus.Subscribe;

/**
 * 监听者：
 */
public class GuavaEvent {

    @Subscribe
    public void aa(String str){
        System.out.println("test ..."+str);
    }
}
