package com.yumingjiang.设计模式.观察者模式.juava_demo;

import com.google.common.eventbus.EventBus;

/**
 * 消息发布以及测试
 */
public class MsgPublishAndTestDemo {
    public static void main(String[] args) {
        EventBus eventBus = new EventBus();

        GuavaEvent guavaEvent = new GuavaEvent();
        eventBus.register(guavaEvent);
        eventBus.post("John");
    }
}
