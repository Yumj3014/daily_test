package com.yumingjiang.设计模式.享元模式;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Vector;

/**
 * 享元模式又称为轻量级模式，是对象池的一种实现、类似于线程池、线程池可以避免平凡的创建和销毁线程带来的消耗性能问题。提供了减少对象数量从而改变应用所需的对象结构的方式，
 * 其宗旨是共享细粒度对象，将多个对同一对象的访问集中起来，不必每个访问者都创建一个对象，以此来降低内存的消耗，属于结构性模式
 */
public class ConnectionPool {

    private Vector<Connection> pool;

    private String url = "jdbc:mysql://localhost:3306/test";
    private String username = "root";
    private String password = "root";
    private String driverClassName = "com.mysql.jdbc.Driver";
    private int poolSize = 100;

    public ConnectionPool() {
        pool = new Vector<Connection>(poolSize);

        try{
            Class.forName(driverClassName);
            for (int i = 0; i < poolSize; i++) {
                Connection conn = DriverManager.getConnection(url,username,password);
                pool.add(conn);
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public synchronized Connection getConnection(){
        if(pool.size() > 0){
            Connection conn = pool.get(0);
            pool.remove(conn);
            return conn;
        }
        return null;
    }

    public synchronized void release(Connection conn){
        pool.add(conn);
    }
}
