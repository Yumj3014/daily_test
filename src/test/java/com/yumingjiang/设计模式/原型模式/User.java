package com.yumingjiang.设计模式.原型模式;

import java.io.*;
import java.util.List;

/**
 * 浅拷贝例子
 */
public class User implements Serializable, Cloneable {
    private String name;
    private String address;
    private List<String> hobbies;

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", hobbies=" + hobbies +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<String> getHobbies() {
        return hobbies;
    }

    public void setHobbies(List<String> hobbies) {
        this.hobbies = hobbies;
    }

    /**
     * 浅克隆
     *
     * @return
     * @throws CloneNotSupportedException
     */
    public User clone() throws CloneNotSupportedException {
        return (User) super.clone();
    }
}
