package com.yumingjiang.设计模式.适配器模式.对象适配器;

/**
 * 对象适配器的实现方式：定义一个目标角色，源角色，适配器
 * 在适配器中将源角色通过对象的方式注入，且在适配器中实现目标角色的接口
 */
public class ObjectAdapter implements DC5{

    private AC220 ac220;

    public ObjectAdapter(AC220 ac220){
        this.ac220=ac220;
    }

    @Override
    public int output5V() {
        int i = ac220.output220() / 44;
        System.out.println("输出："+i+"V电压");
        return i;
    }
}