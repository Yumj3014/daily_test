package com.yumingjiang.设计模式.适配器模式.对象适配器;

public class Test {
    public static void main(String[] args) {
        ObjectAdapter adapter = new ObjectAdapter(new AC220());
        adapter.output5V();
    }
}