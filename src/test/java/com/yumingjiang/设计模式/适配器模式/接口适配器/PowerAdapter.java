package com.yumingjiang.设计模式.适配器模式.接口适配器;

/**
 * <h3>study</h3>
 * <p></p>
 * 接口适配器：违背了单一职责原则和接口职责原则
 * @author : yumin
 * @date : 2021-04-01 06:17
 **/
public class PowerAdapter implements DC {
    private AC220 ac220;
    public PowerAdapter(AC220 ac220){
        this.ac220=ac220;
    }
    @Override
    public int output5V() {
        int i = ac220.output220() / 44;
        System.out.println("输出："+i+"V电压");
        return i;
    }
    @Override
    public int output12V() {
        return 0;
    }

    @Override
    public int output24V() {
        return 0;
    }

    @Override
    public int output36V() {
        return 0;
    }

    @Override
    public int output48V() {
        return 0;
    }
}
