package com.yumingjiang.设计模式.适配器模式.例子.adapterv2.adapters;

import com.yumingjiang.设计模式.适配器模式.例子.PassportService;
import com.yumingjiang.设计模式.适配器模式.例子.ResultMsg;

/**
 * Created by Tom.
 */
public abstract class AbstraceAdapter extends PassportService implements ILoginAdapter {
    protected ResultMsg loginForRegist(String username, String password){
        if(null == password){
            password = "THIRD_EMPTY";
        }
        super.regist(username,password);
        return super.login(username,password);
    }
}
