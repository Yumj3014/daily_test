package com.yumingjiang.设计模式.适配器模式.例子.adapterv2.adapters;

import com.yumingjiang.设计模式.适配器模式.例子.ResultMsg;

public class LoginForWechatAdapter extends AbstraceAdapter{
    public boolean support(Object adapter) {
        return adapter instanceof LoginForWechatAdapter;
    }

    public ResultMsg login(String id, Object adapter) {
        return super.loginForRegist(id,null);
    }


}
