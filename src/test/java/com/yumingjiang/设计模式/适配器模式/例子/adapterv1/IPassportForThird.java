package com.yumingjiang.设计模式.适配器模式.例子.adapterv1;

import com.yumingjiang.设计模式.适配器模式.例子.ResultMsg;

public interface IPassportForThird {

    ResultMsg loginForQQ(String openId);

    ResultMsg loginForWechat(String openId);

    ResultMsg loginForToken(String token);

    ResultMsg loginForTelphone(String phone, String code);

}
