package com.yumingjiang.设计模式.适配器模式.类适配器;

/**
 * 类适配器的实现思路：定义一个目标角色，源角色，适配器中实现目标角色的接口，
 * 在目标角色的接口中讲源角色转化成目标角色
 */
public class ClassAdpter5V extends AC220 implements DC5 {
    @Override
    public int output5V() {
        int output = super.output220() / 44;
        System.out.println("输出：" + output + "5V.");
        return output;
    }
}