package com.yumingjiang.设计模式.适配器模式.类适配器;

/**
 * <h3>study</h3>
 * <p></p>
 *  类适配器：可以实现适配的功能，但是有个缺点就是由于继承的原因导致如果我们使用适配器
 *  来调用继承类的方法的方法的话，也是可以调用出来的，所以违背了最少指导原则
 * @author : yumin
 * @date : 2021-04-01 06:04
 **/
public class Test {
    public static void main(String[] args) {
        ClassAdpter5V adpter5V = new ClassAdpter5V();
        adpter5V.output5V();
        adpter5V.output220();
    }
}