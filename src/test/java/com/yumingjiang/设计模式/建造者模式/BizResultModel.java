package com.yumingjiang.设计模式.建造者模式;

import lombok.Data;

@Data
public class BizResultModel {
    private Integer code;
    private String msg;
    private String bizline;
    private String owner;
    private String desc;
    private String version;
    public Integer getCode() {
        return code;
    }
}