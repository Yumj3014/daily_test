package com.yumingjiang.设计模式.建造者模式;

/**
 * 测试建造者模式
 */
public class TestBuilderPattern {
    public static void main(String[] args) {
        BizResultModel bizResultModel = new BizResultModelBuilder()
                .addCode(200)
                .addBizline("Datahub")
                .addDesc("test")
                .addOwner("john")
                .build();
        System.out.println(bizResultModel);
    }
}