package com.yumingjiang.设计模式.建造者模式;

public class BizResultModelBuilder {
    private BizResultModel bizResultModel;

    public BizResultModelBuilder() {
        bizResultModel = new BizResultModel();
    }

    public BizResultModelBuilder addCode(Integer code) {
        this.bizResultModel.setCode(code);
        return this;
    }

    public BizResultModelBuilder addBizline(String bizline) {
        this.bizResultModel.setBizline(bizline);
        return this;
    }

    public BizResultModelBuilder addOwner(String owner) {
        this.bizResultModel.setOwner(owner);
        return this;
    }

    public BizResultModelBuilder addDesc(String desc) {
        this.bizResultModel.setDesc(desc);
        return this;
    }

    public BizResultModel build() {
        return this.bizResultModel;
    }
}