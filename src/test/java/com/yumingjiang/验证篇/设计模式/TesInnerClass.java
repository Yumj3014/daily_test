package com.yumingjiang.验证篇.设计模式;


import com.yumingjiang.设计模式.单例模式.SimpleLazySingleton03;

import java.lang.reflect.Constructor;

/**
 * <h3>设计模式测试</h3>
 * <p>验证静态内部类单例模式被反射破坏</p>
 *
 * @author : yumin
 * @date : 2021-03-20 08:53
 **/
public class TesInnerClass {
    public static void main(String[] args) {
        SimpleLazySingleton03 instance1 = SimpleLazySingleton03.getInstance();
        System.out.println(instance1);

        //测试通过反射破坏单例
        //1、获取类对象
        Class<SimpleLazySingleton03> clazz = SimpleLazySingleton03.class;
        try {
            //2、获取该类的构造方法
            Constructor<SimpleLazySingleton03> constructor = clazz.getDeclaredConstructor();
            //3、强制访问
            constructor.setAccessible(true);
            //4、通过构造方法实例化对象
            SimpleLazySingleton03 instance = constructor.newInstance();
            System.out.println(instance);
            System.out.println(instance==instance1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}